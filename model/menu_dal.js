var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = 'SELECT * FROM menu ' +
                'ORDER BY menu_name ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(menu_id, callback) {
    var query = 'SELECT m.*, r.recipe_name, r.recipe_id FROM menu m ' +
        'LEFT JOIN compose c on m.menu_id = c.menu_id ' +
        'LEFT JOIN recipe r on r.recipe_id = c.recipe_id ' +
        'WHERE m.menu_id = ?';
    var queryData = [menu_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

var recipeMenuInsert = function(menu_id, recipeIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO compose (menu_id, recipe_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var recipeMenuData = [];
    if (recipeIdArray.constructor == Array) {
        for (var i=0; i < recipeIdArray.length; i++) {
            recipeMenuData.push([menu_id, recipeIdArray[i]])
        }
    }
    else {
        recipeMenuData.push([menu_id, recipeIdArray]);
    }
    connection.query(query, [recipeMenuData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.recipeMenuInsert = recipeMenuInsert;

exports.insert = function(params, callback) {

    // FIRST INSERT THE menu
    var query = 'INSERT INTO menu (menu_name, description) ' +
        'VALUES (?, ?)';
    var queryData = [params.menu_name, params.description];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE INSERT_ID THAT RETURNED AS menu_ID
        var menu_id = result.insertId;

        if (params.recipe_id != null) {
            recipeMenuInsert(menu_id, params.recipe_id, function (err, result) {
            });
        }

        callback(err, menu_id);

    });

};


//declare the function so it can be used locally
var recipeMenuDelete = function(menu_id, callback){
    var query = 'DELETE FROM compose WHERE menu_id = ?';
    var queryData = [menu_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.recipeMenuDelete = recipeMenuDelete;

exports.update = function(params, callback) {
    var query = 'UPDATE menu SET menu_name = ?, ' +
        'description = ? ' +
        'WHERE menu_id = ?';
    var queryData = [params.menu_name, params.description, params.menu_id];

    connection.query(query, queryData, function(err, result) {
        //delete compose entries for this menu
        recipeMenuDelete(params.menu_id, function(err, result){
            if(params.recipe_id != null) {
                //insert recipe ids
                recipeMenuInsert(params.menu_id, params.recipe_id, function(err, result){
                    if (err) {
                        callback(err, result);
                    }
                });
            }
        });
        callback(err, result);
    });
};

exports.delete = function(menu_id, callback) {
    var query = 'DELETE FROM menu WHERE menu_id = ?';
    var queryData = [menu_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};