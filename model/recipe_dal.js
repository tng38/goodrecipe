var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = 'SELECT * FROM recipe ' +
                'ORDER BY recipe_name ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
DROP PROCEDURE IF EXISTS recipe_getInfo;
DELIMITER //
CREATE PROCEDURE recipe_getInfo(_recipe_id int)
BEGIN
	SELECT *
    FROM recipe r
    WHERE r.recipe_id = _recipe_id;

	SELECT i.*
	FROM recipe r
	JOIN ingredient i ON r.recipe_id = i.recipe_id
	WHERE r.recipe_id = _recipe_id
    ORDER BY ingredient_name;

    SELECT t.*
    FROM recipe r
    JOIN recipe_type rt ON r.recipe_id = rt.recipe_id
    JOIN type t ON t.type_id = rt.type_id
    WHERE r.recipe_id = _recipe_id
    ORDER BY type_name;

END //
DELIMITER ;

CALL recipe_getInfo(1);

 */

exports.getById = function(recipe_id, callback) {
    var query = 'CALL recipe_getInfo(?)';
    var queryData = [recipe_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*
// a function return total time needed for a recipe
DROP FUNCTION IF EXISTS fn_recipe_get_time;
DELIMITER //
CREATE FUNCTION fn_recipe_get_time(_recipe_id INT) RETURNS TIME
BEGIN
DECLARE _time TIME;
SELECT  ADDTIME(ADDTIME(inactive_time, prep_time),cook_time) INTO _time
FROM recipe
WHERE recipe_id = _recipe_id;

RETURN _time;

END //
DELIMITER ;

/-- create a new view of recipe table with total time needed
CREATE OR REPLACE VIEW recipe_time_view AS
SELECT recipe_id, recipe_name,  creator, fn_recipe_get_time(recipe_id) recipe_time
FROM recipe;

SELECT * from recipe_time_view;

*/


exports.getByFilter = function(params, callback) {
    var query = 'SELECT DISTINCT r.recipe_id, recipe_name FROM recipe_time_view r ' +
        'JOIN ingredient g ON g.recipe_id = r.recipe_id ' +
        'JOIN recipe_type rt ON rt.recipe_id = r.recipe_id ' +
        'JOIN type t	ON t.type_id = rt.type_id ';

    var queryWhere = "";
    var queryData = [];

    if (params.recipe_name.trim() != "") {
        queryWhere = "recipe_name LIKE ? ";
        var s = "%" + params.recipe_name.trim() + "%";
        queryData.push(s);
    }
    if (params.creator.trim() != "") {
        if (queryWhere != "")
            queryWhere += " AND ";
        queryWhere += "creator LIKE ? ";
        var s = "%" + params.creator.trim() + "%";
        queryData.push(s);
    }
    if (params.ingredient.trim() != "") {
        if (queryWhere != "")
            queryWhere += " AND ";
        queryWhere += "ingredient_name LIKE ? ";
        var s = "%" + params.ingredient.trim() + "%";
        queryData.push(s);
    }
    if (params.recipe_hour.trim() != "" || params.recipe_minute.trim() != "") {
        var hour = params.recipe_hour.trim();
        var minute = params.recipe_minute.trim();
        var time_limit = hour + ":" + minute + ":00";
        if (queryWhere != "")
            queryWhere += " AND ";
        queryWhere += "recipe_time <=  ? ";
        queryData.push(time_limit);
    }
    if (params.type_id != null) {
        var queryOr = "";
        for (var i=0; i < params.type_id.length; i++) {
            if (queryOr != "") {
                queryOr += " OR "
            }
            queryOr += "t.type_id = ? "
            queryData.push(params.type_id[i])
        }
        if (queryWhere != "")
            queryWhere += " AND ";
        queryWhere = queryWhere + "( " + queryOr + " ) ";
    }
    if (queryWhere != "") {
        query = query + "WHERE " + queryWhere;
    }

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.getLess = function(numOfIngrdient, callback) {
    var query = 'SELECT COUNT(ingredient_name) count, r.recipe_id, recipe_name ' +
        'FROM recipe r ' +
        'JOIN ingredient i ON r.recipe_id = i.recipe_id ' +
        'GROUP BY i.recipe_id ' +
        'HAVING count < ? ' +
        'ORDER BY recipe_name ';

    queryData = [numOfIngrdient];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });

};

exports.getNoIngredient = function(ingredient_name, callback) {
    var query = 'SELECT recipe_id, recipe_name ' +
        'FROM recipe r ' +
        'WHERE NOT EXISTS (SELECT i.recipe_id FROM ingredient i ' +
        'WHERE r.recipe_id = i.recipe_id AND ingredient_name LIKE ?) ';
    var ingredientData = "%" + ingredient_name.trim() + "%";
    queryData = [ingredientData];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });

};

var ingredientInsert = function(recipe_id, ingredient_name, callback) {
    // insert the ingredient
    var query = 'INSERT  INTO ingredient (recipe_id, ingredient_name) ' +
        'VALUES (?, ?) ';
    var queryData = [recipe_id, ingredient_name];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.ingredientInsert = ingredientInsert;

var recipeTypeInsert = function(recipe_id, typeIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO recipe_type (recipe_id, type_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var recipeTypeData = [];
    if (typeIdArray.constructor == Array) {
        for (var i=0; i < typeIdArray.length; i++) {
            recipeTypeData.push([recipe_id, typeIdArray[i]])
        }
    }
    else {
        recipeTypeData.push([recipe_id, typeIdArray]);
    }
    connection.query(query, [recipeTypeData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.recipeTypeInsert = recipeTypeInsert;

exports.insert = function(params, callback) {

    // FIRST INSERT THE recipe
    var query = 'INSERT INTO recipe (recipe_name, creator, description, instruction, prep_time, cook_time, inactive_time) ' +
        'VALUES (?, ?, ?, ?, ?, ?, ?)';
    var prep_time = params.prep_hour + ":" + params.prep_minute + ":00";
    var cook_time = params.cook_hour + ":" + params.cook_minute + ":00";
    var inactive_time = params.inactive_hour + ":" + params.inactive_minute + ":00";


    var queryData = [params.recipe_name, params.creator, params.description, params.instruction, prep_time, cook_time, inactive_time];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE INSERT_ID THAT RETURNED AS recipe_ID
        var recipe_id = result.insertId;

        if (params.ingredient1.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient1, function (err, result) {
            });
        }
        if (params.ingredient2.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient2, function (err, result) {
            });
        }
        if (params.ingredient3.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient3, function (err, result) {
            });
        }
        if (params.ingredient4.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient4, function (err, result) {
            });
        }
        if (params.ingredient5.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient5, function (err, result) {
            });
        }
        if (params.ingredient6.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient6, function (err, result) {
            });
        }
        if (params.ingredient7.trim() != "") {
            ingredientInsert(recipe_id, params.ingredient7, function (err, result) {
            });
        }

        if (params.type_id != null) {
            recipeTypeInsert(recipe_id, params.type_id, function (err, result) {
            });
        }

        callback(err, recipe_id);

    });

};

exports.edit = function(recipe_id, callback) {
    var query = 'CALL recipe_getInfo(?)';
    var queryData = [recipe_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//declare the function so it can be used locally
var recipeTypeDelete = function(recipe_id, callback){
    var query = 'DELETE FROM recipe_type WHERE recipe_id = ?';
    var queryData = [recipe_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.recipeTypeDelete = recipeTypeDelete;

//declare the function so it can be used locally
var recipeIngredientDelete = function(recipe_id, callback){
    var query = 'DELETE FROM ingredient WHERE recipe_id = ?';
    var queryData = [recipe_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.recipeIngredientDelete = recipeIngredientDelete;

exports.update = function(params, callback) {
    var query = 'UPDATE recipe SET recipe_name = ?, creator = ?, ' +
        'description = ?, instruction = ?, ' +
        'prep_time = ?, cook_time = ?, inactive_time = ? ' +
        'WHERE recipe_id = ?';
    var prep_time = params.prep_hour + ":" + params.prep_minute + ":00";
    var cook_time = params.cook_hour + ":" + params.cook_minute + ":00";
    var inactive_time = params.inactive_hour + ":" + params.inactive_minute + ":00";
    var queryData = [params.recipe_name, params.creator, params.description,
        params.instruction, prep_time, cook_time, inactive_time, params.recipe_id];

    connection.query(query, queryData, function(err, result) {
        recipeIngredientDelete(params.recipe_id, function(err, result) {
            if (params.ingredient1.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient1, function (err, result) {
                });
            }
            if (params.ingredient2.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient2, function (err, result) {
                });
            }
            if (params.ingredient3.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient3, function (err, result) {
                });
            }
            if (params.ingredient4.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient4, function (err, result) {
                });
            }
            if (params.ingredient5.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient5, function (err, result) {
                });
            }
            if (params.ingredient6.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient6, function (err, result) {
                });
            }
            if (params.ingredient7.trim() != "") {
                ingredientInsert(params.recipe_id, params.ingredient7, function (err, result) {
                });
            }
        });

        //delete recipe_Type entries for this recipe
        recipeTypeDelete(params.recipe_id, function(err, result){
            if(params.type_id != null) {
                //insert type ids
                recipeTypeInsert(params.recipe_id, params.type_id, function(err, result){
                    if (err) {
                        callback(err, result);
                    }
                });
            }
        });
        callback(err, result);
    });
};

exports.delete = function(recipe_id, callback) {
    var query = 'DELETE FROM recipe WHERE recipe_id = ?';
    var queryData = [recipe_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};